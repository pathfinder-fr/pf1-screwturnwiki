<div class="presentation navmenudroite">
===Les archétypes d'occultiste===
&nbsp;Retour à la [[Occultiste|classe]].
<ul style="margin-top:2px"><li>[[Dévoreur de livres (Occultiste)|Dévoreur de livres]] (AO)</li>
  <li>[[Hôte de guerre (Occultiste)|Hôte de guerre]] (AO)</li>
  <li>[[Nécroccultiste (Occultiste)|Nécroccultiste]] (AO)</li>
  <li>[[Reliquaire (occultiste)|Reliquaire]] (''OO'')</li>
  <li>[[Shair (Occultiste)|Sha'ir]] (AO)</li>
</ul>
</div>