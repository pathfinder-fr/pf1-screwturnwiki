---
Name: Pathfinder2.ADJ
Title: Aides de jeu pour Pathfinder 2
LastModified: 2022-05-02 14:53
---
• [[MainPage|Retour à la page principale pour Pathfinder 2]] <br/>

{toc}

=== Création et feuilles de personnage ===
'''IMPORTANT''' Pour pouvoir éditer une feuille de personnage au format Google Sheet, commencez par vous en créer une copie (Menu File/Fichier, option Create a Copy).

''En français :''
* Feuille de perso formulaire PDF par Enrique : [[https://www.pathfinder-fr.org/Wiki/public/Upload/ADJ/PF2/PF2-formulaire.pdf|fichier PDF éditable]] ; ainsi qu'une [[https://www.pathfinder-fr.org/Wiki/public/Upload/ADJ/PF2/PF2-ficheAlchimie.pdf|fiche spécifique pour les alchimistes]].

* Feuille de perso orientation paysage : [[https://www.pathfinder-fr.org/Wiki/public/Upload/ADJ/PF2/PF2CSv9.pdf|version couleur]] [[https://www.pathfinder-fr.org/Wiki/public/Upload/ADJ/PF2/PF2CSv9NB.pdf|version NB]]

* Google Sheet par FireAndStone, traduite par P-FR : [[https://docs.google.com/spreadsheets/d/1NiAnhgtLXGFTt4P-u7YIb5MMDk9A1nB7rw_U--aOdoI/edit|lien]] - voir '''IMPORTANT''' ci-dessus !

* Google Sheet par scientificbruno, traduite et étendue par eyrafr : [[https://docs.google.com/spreadsheets/d/1sB-hZhbl0BGWvl66Fat4e-3A3Wi_8LX4VxmexKeX5jo/edit|version jusqu'au niveau 10]] ou [[https://docs.google.com/spreadsheets/d/1bI9Gv4Q2FpOi7uK-PPAIunQhTjIZm194NFmp-eZakSc/edit|version jusqu'au niveau 20]] - voir '''IMPORTANT''' ci-dessus !

* Feuille "compressée" par Bellerophon : [[https://www.pathfinder-fr.org/Wiki/public/Upload/ADJ/PF2/FDP Bellerophon.pdf|fichier PDF]],<br/>ainsi que des versions éditables par Vel Cheran : [[https://www.pathfinder-fr.org/Wiki/public/Upload/ADJ/PF2/FDP edit.pdf|fichier PDF]] et [[https://www.pathfinder-fr.org/Wiki/public/Upload/ADJ/PF2/FDP edit liens.pdf|avec copie automatique des modificateurs]]<br/>et des versions mises à jour par Vel Cheran : [[https://www.pathfinder-fr.org/Wiki/public/Upload/ADJ/PF2/Fperso VelCharan corrigee.xlsx|fichier Excel]] ou [[https://www.pathfinder-fr.org/Wiki/public/Upload/ADJ/PF2/Fperso VelCharan corrigee.ods|fichier LibreOffice]]

* Feuille "rétro" en un recto + un verso par Wheldrake : [[https://www.pathfinder-fr.org/Wiki/public/Upload/ADJ/PF2/retrosheetv3.pdf|version PDF]] ou [[https://www.pathfinder-fr.org/Wiki/public/Upload/ADJ/PF2/retrosheetv3.odg|version ODG (Libre Office)]]

* Fiche de perso en plusieurs volets par Fingen : [[https://www.pathfinder-fr.org/Wiki/public/Upload/ADJ/PF2/PF2-Fiche_de_perso-Fingen-Smarties.pdf|version "smarties"]], [[https://www.pathfinder-fr.org/Wiki/public/Upload/ADJ/PF2/PF2-Fiche_de_perso-Fingen-SmartiesLight.pdf|version "smarties light"]] ou [[https://www.pathfinder-fr.org/Wiki/public/Upload/ADJ/PF2/PF2-Fiche_de_perso-Fingen-SmartiesNB.pdf|version noir & blanc]] ; [[https://www.pathfinder-fr.org/Wiki/public/Upload/ADJ/PF2/PF2-Fiche_de_perso-Fingen-Smarties_v2.pdf|v2 en format A5]] — [[https://www.pathfinder-fr.org/Wiki/public/Upload/ADJ/PF2/PF2-compagnons_et_sortsprimordiaux.pdf|insert pour les sorts primordiaux et compagnons animaux]]

* Feuille de perso étendue très complète par Hemdejy : [[https://www.pathfinder-fr.org/Wiki/public/Upload/ADJ/PF2/PF2_Feuille-de-perso-etendue.pdf|fichier PDF]]

''En anglais :''
* Feuille de perso officielle, PDF modifiable : [[/Wiki/public/Upload/ADJ/PF2/PF2 Fillable.pdf|fichier PDF]]

* Google Sheet par Charon : [[https://docs.google.com/spreadsheets/d/1-9Nr0Qi5CMQxj3Z3tAHXR8UmSisuFOOp9OsgIxh0sMs/edit|Lien]] - voir '''IMPORTANT''' ci-dessus !

* Google Sheet "feuille simplifiée" : [[https://docs.google.com/spreadsheets/d/137VrNsuxzHlopGf_O-l_Fbn41-kN_1jbplouCoomwUU/edit|lien]] - voir '''IMPORTANT''' ci-dessus !

* Version rétro par BlueJay64 : [[https://drive.google.com/file/d/1Yfh7BbXSRTZ1_CEEytDv6ut_e5XM7vhi/view|lien vers PDF]]

* Site de création et de gestion de personnages : [[http://character.pf2.tools/|PF2 Tools]]

''Guides :''
* Guide des étapes de la création d'un personnage (+ numéros de pages du Livre de Base VF) par Enrique : [[/Wiki/public/Upload/ADJ/PF2/creapersoPF2.pdf|fichier PDF]] (MàJ : 18 mars 2021)


=== Prétirés en français ===

Merci à Azmanül pour avoir traduit et mis en page les prétirés suivants !
* [[{UP}/ADJ/PF2/Amiri niv1.pdf|Amiri]], barbare niveau 1
* [[/Wiki/public/Upload/ADJ/PF2/Ezren niv1.pdf|Ezren]], magicien niveau 1
* [[/Wiki/public/Upload/ADJ/PF2/Fumbus niv1.pdf|Fumbus]], alchimiste niveau 1
* [[/Wiki/public/Upload/ADJ/PF2/Harsk niv1.pdf|Harsk]], rôdeur niveau 1
* [[/Wiki/public/Upload/ADJ/PF2/Kyra niv1.pdf|Kyra]], prêtresse niveau 1
* [[{UP}/ADJ/PF2/Lem niv1.pdf|Lem]], barde niveau 1
* [[/Wiki/public/Upload/ADJ/PF2/Lini niv1.pdf|Lini]], druide niveau 1
* [[/Wiki/public/Upload/ADJ/PF2/Merisiel niv1.pdf|Mérisiel]], roublarde niveau 1
* [[/Wiki/public/Upload/ADJ/PF2/Sajan niv1.pdf|Sajan]], moine niveau 1
* [[/Wiki/public/Upload/ADJ/PF2/Seelah niv1.pdf|Seelah]], championne niveau 1
* [[/Wiki/public/Upload/ADJ/PF2/Séoni niv1.pdf|Séoni]], ensorceleuse niveau 1
* [[/Wiki/public/Upload/ADJ/PF2/Valeros niv1.pdf|Valéros]], guerrier niveau 1

L'ensemble des personnages niveau 1 est téléchargeable [[/Wiki/public/Upload/ADJ/PF2/Personnages pré-tirés niv1 PF2.zip|ici]].



=== Aides de jeu génériques ===

* Compilation des actions (fichier PDF) par Zerr0wFPS : [[https://www.pathfinder-fr.org/Wiki/public/Upload/ADJ/PF2/Actions_PF2.pdf|fichier PDF]]

* Compilation des états (fichier PDF) par Zerr0wFPS : [[https://www.pathfinder-fr.org/Wiki/public/Upload/ADJ/PF2/Etats_PF2.pdf|fichier PDF]]

* Liste des actions (fichier PDF) : [[https://www.pathfinder-fr.org/Wiki/public/Upload/ADJ/PF2/PF2Actions.pdf|Version simplifiée]] - [[https://www.pathfinder-fr.org/Wiki/public/Upload/ADJ/PF2/Actionsv3.pdf|Version complète]]

* Résumé des traits (d'actions, d'armes, ...) : [[https://www.pathfinder-fr.org/Wiki/public/Upload/ADJ/PF2/PF2TraitsObjets.pdf|Fichier PDF]]

* Une cinquantaine de conseils : [[50conseils]]


=== Règles ===

* Traduction de la preview de l'Advanced Player's Guide sur l'héritage générique de dhampir : [[https://www.pathfinder-fr.org/Wiki/public/Upload/ADJ/PF2/Dhampir.pdf|Fichier PDF]]

* Traduction du [[PlaytestGG|playtest "Guns & Gears"]] avec l'inventeur et le pistolero

* Explications de règles : [[Règles-Artisanat|L'artisanat]]

* Un aperçu du système "des [[Parties-Monstres|parties de monstres]]" introduit dans le Battlezoo Bestiary

* Un résumé d'introduction aux règles par Resorb : [[/Wiki/public/Upload/ADJ/PF2/PF2 initiation règles.pdf|Fichier PDF]]

* {s:new} {s:Reference|BOTD}Quelques règles provenant des previews du Book of the Dead :
** [[PF2 BotD Jouer un mort-vivant|Jouer un mort-vivant]] : [[PF2 BotD Fantôme|Fantôme]] [[PF2 BotD Goule|Goule]] [[PF2 BotD Liche|Liche]] [[PF2 BotD Momie|Momie]] [[PF2 BotD Vampire|Vampire]] [[PF2 BotD Zombi|Zombi]] (archétypes), [[PF2 BotD Squelette|Squelette]] (ascendance)
** Archétypes pour vivants : [[PF2 BotD Gardien des âmes|Gardien des âmes]], [[PF2 BotD Nécromancien sacré|Nécromancien sacré]], [[PF2 BotD Tueur de morts-vivants|Tueur de morts-vivants]]


=== Aides de jeu relatives aux publications ===

* Compilation des errata du Livre de base VF par Rankkor : [[https://www.pathfinder-fr.org/Wiki/public/Upload/ADJ/PF2/Errata_LdB.pdf|Fichier PDF]]


=== Liens utiles ===

* SRD officiel en anglais : [[http://2e.aonprd.com/Rules.aspx|Archives of Nethys]]

* Autre SRD : [[https://pf2.d20pfsrd.com/|d20pfsrd]]

* Banques de données en ligne : [[http://pf2.easytool.es/index.php|Easytool]] - [[http://pf2.easytool.es/tree/|Easytool version arbre]] - [[http://pf2.easytool.es/sheets/|Easytool listes de sorts]]

* Autre banque de données en ligne : [[http://pathfinder-srd.herokuapp.com/|Hero KU]]

* Spellfinder : [[https://spellfinder.joffrey.eu/|lien]]

* Site général regroupant toute une série d'informations, de documents et de liens : [[http://pf2.tools/|PF2 Tools]]

* Application mobile pour la construction de personnages : [[https://play.google.com/store/apps/details?id=com.redrazors.pathbuilder2e&hl=en|Pathbuilder]]

* Utiliser l'application mobile pour la construction de personnages sur PC ? 
- Installer [[https://www.bluestacks.com/fr/index.html|BlueStack]] Emulateur Androïd pour PC,
- Télécharger [[https://play.google.com/store/apps/details?id=com.redrazors.pathbuilder2e&hl=en|Pathbuilder]] à partir d'un compte Google
- Aller dans les Réglages de BlueStack > Avancé > Profil de l'appareil > Choisir un profil prédéfini : Samsung Galaxy S10 > Enregistrer 


=== Documents et liens pour MJ ===

* Guide de conversion et de création d'aventures en Pathfinder 2 (y compris les règles pour la création de monstres, PNJ et pièges du *Guide de maîtrise*) : [[https://www.pathfinder-fr.org/Wiki/public/Upload/ADJ/PF2/ConvCréa PF2.pdf|Fichier PDF]]

* Site pour créer des monstres en ligne : [[http://monster.pf2.tools/|Monster Tools]]

* Site permettant de générer divers éléments aléatoires pour PF2 (dont des tables de loot) : [[https://www.leitknightgaming.com/loot2|Leitknight Gaming]]

* Google Sheet permettant de modifier le niveau d'un monstre donné : [[https://docs.google.com/spreadsheets/d/18zsPLqWy8CLnGbCcGgaR8cT55qnedhTBE_rspnLqA0Q/edit#gid=2007578940|Lien]]

* Google Sheet permettant de prévoir les rencontres et l'évolution en XP des personnages : [[https://docs.google.com/spreadsheets/d/147Qwk0-nl1tvDNCyYBzXXskMAaU4MQJ53RYa4qVEIbI/edit|Lien]]

* Document Word proposant un formatage pour les monstres, objets et pièges : [[https://www.pathfinder-fr.org/Wiki/public/Upload/ADJ/PF2/2e_template.docx|Lien]]

* Guide de conversion PF1 -> PF2 par Eliwir, avec conseils et exemples : [[https://docs.google.com/document/d/17XuFTsO9yHFWZZuWvdGuc0iQJoEvZVlsjicUWlB8nVk/edit|Lien Google Doc]]

* Application mobile pour la construction de rencontres : [[https://play.google.com/store/apps/details?id=de.enduni.monsterlair|MonsterLair]]

* Gabarits pour les effets de zone utilisables avec Roll20 par Elmios : [[https://www.pathfinder-fr.org/Wiki/public/Upload/ADJ/PF2/GabaritsRoll20.zip|Fichier Zip]]

* Table de rencontres aléatoire au Mwangi par Jason Tondro (développeur Pathfinder et Starfinder) : [[rencontresKaava|Lien]]


=== Boîte à outils du MJ ===

* [[CouloirTombal|Le couloir tombal]] (Piège 4)
* [[PerduLabyrinthe|Perdu dans le labyrinthe]]
* [[KoboldsEnApproche|Kobold en approche !]] (Rencontre modérée 4)
* [[MasquesGardiensSecrets|Les masques des gardiens des secrets]] (Objet 3)
* [[UrokInfiltrateurKobold|Urok, infiltrateur kobold]] (PNJ 9)
* [[CheaDeploiementVents|Chéa, étudiante du déploiement des vents]] (PNJ 9)
* [[Phinelli|L'élixir miraculeux de Phinelli]] (Rencontre difficile 3)
* [[GardienDivin|Gardien divin de Brigh]] (Créature 10 - monstre bonus pour le Bestiaire 3)
