---
Name: Pathfinder2.Publications
Title: Publications relatives à Pathfinder 2e édition
LastModified: 2022-04-05 04:14
---
{s:Image|Logos/PFS2/PF2Logo.png|480px|fright}
• [[MainPage|Retour à la page principale pour Pathfinder 2]] <br/>

{TOC}

===Règles et livres de base===
{s:LivrePF2|http://paizo.com/image/product/catalog/PZO/PZO2101_180.jpeg|Core Rulebook / Livre de base}
Ce guide complet de 640 pages fournit tout ce dont vous avez besoin pour vous lancer dans un monde d'aventure fantastique sans limites! Choisissez parmi des ascendances comme les elfes, les humains et les gobelins et des classes comme l'alchimiste, le combattant et l'ensorceleur pour créer un héros de votre propre conception, destiné à devenir une légende! Les nouvelles règles sont plus faciles à apprendre et plus rapides à jouer, et elles offrent une personnalisation plus approfondie que jamais!

Ce volume indispensable contient les règles de base pour les joueurs et les Maîtres du Jeu, et est votre première étape dans un nouveau voyage héroïque!

Date de sortie US : 01/08/2019 — Date de sortie FR : 20/12/2019

{s:LivrePF2|https://cdn.paizo.com/image/product/catalog/PZO/PZO2103_180.jpeg|Gamemastery Guide / Guide du maître}
Que vous soyez un maître du jeu novice ou expérimenté, vous pouvez toujours trouver de nouvelles façons d'affiner votre maîtrise du jeu. Ce livre de règles de 256 pages contient une multitude de nouvelles informations, outils et règles à ajouter à votre jeu. À l'intérieur de ce livre, vous trouverez des conseils pratiques pour construire vos propres aventures, concevoir des villes et créer des personnages. Vous trouverez un système de règles pour des courses poursuites dramatiques, organiser des tournois et des duels mortels. Ce livre comprend également plus de 40 pages d'exemples de personnages non joueurs, du simple garde de la ville au cultiste maléfique.

Date de sortie US : 26/02/2020 — Date de sortie FR : 14/12/2020

{s:LivrePF2|https://cdn.paizo.com/image/product/catalog/PZO/PZO2105_180.jpeg|Advanced Player's Guide}
Repoussez les limites de ce qui est possible avec le Guide du joueur avancé de Pathfinder! Ce livret de règles  de 272 pages contient de nouvelles options de règles passionnantes pour les joueurs, ajoutant encore plus de choix! À l'intérieur, vous trouverez de nouvelles ascendances, et quatre nouvelles classes: l'investigateur, l'oracle, le bretteur et la sorcière ! Le guide du joueur avancé comprend également de nouvelles options pour toutes les classes du livre de base et plein de nouveaux historiques, dons généraux, sorts, objets et 40 archétypes pour personnaliser votre expérience de jeu!.

Date de sortie US : 30/07/2020 — Date de sortie FR : Octobre 2021

{s:LivrePF2|https://cdn.paizo.com/image/product/catalog/PZO/PZO2106_180.png|Pathfinder Beginner Box / Boîte d'initiation}
Faites vos premiers pas dans un monde merveilleux d'aventures fantastiques ! La boîte d'initiation à Pathfinder contient tout ce dont vous avez besoin pour apprendre comment jouer au jeu de rôle Pathfinder, y compris des règles pour créer votre propre héros fantastiques et des outils pour créer vos propres histoires. Serez-vous un courageux guerrier qui chargera au combat ou un roublard sournois qui se déplacera sans bruit pour frapper ses ennemis depuis les ombres ? Ou peut-être serez-vous un magicien érudit capable de manipuler d'incroyables sorts ou un prêtre sage et pieux utilisant les pouvoirs de son dieu pour façonner le monde en quelque chose de meilleur ? Tout cela ne dépend que de vous !

À l'intérieur, vous trouverez une vaste boîte à outil remplie de tout ce dont vous avez besoin pour commencer, y compris des règles pour la création de personnages, une aventure d'introduction, des conseils pour construire votre propre campagne, des feuilles de personnage, une carte en couleurs, des pions pour les personnages et de dés. Cette boîte de luxe est une introduction idéale au jeu de rôle Pathfinder qui ouvre la voir à une vie entière d'aventures !

La boîte contient : un Livret du héros de 80 pages comprenant des règles pour la création de personnages, une aventure solo, des compétences, des sorts et de l'équipement ; un Manuel du maître du jeu de 96 pages comprenant une aventure d'introduction, plus de 20 pages de monstres, des règles pour bâtir votre propre aventure et des objets magiques ; quatre personnages préconstruits pour vous permettre de vous lancer immédiatement dans le jeu et quatre feuilles de personnages vierges si vous voulez bâtir vos propres héros ; plus de 100 pions pour personnages et monstres que vous pourrez utiliser sur la carte en couleurs double-face ; 4 cartes de référence pour aider les joueurs à se souvenir de leurs actions ; un set complet de dés y compris un d20, un d12, un d10, un d8, un d6 et un d4.

Date de sortie US : 15/10/2020 — Date de sortie FR : Juin 2021


{s:ClearRight}
===Règles étendues===
{s:LivrePF2|https://cdn.paizo.com/image/product/catalog/PZO/PZO2108_180.png|Secrets of Magic}
Découvrez la puissance inédite de la magie ! Secrets of Magic amène en seconde édition les classes du magus et du conjurateur, ouvrant la voie à des héros qui combinent puissance magique et prouesses martiales et permettant de contrôler une puissante créature magique comme compagnon. Ce livre de 256 pages garni de nombreuses illustrations présente des centaines de nouveaux sorts qui intéresseront toutes les classes capables d'utiliser la magie, des objets magiques pour tous les personnages ainsi que des explications détaillant la structure fondamentale et les théories de la magie. Une section spéciale au sein de ce livre, le "Livre de la magie illimitée", présente de nouvelles méthodes pour lancer des sorts, comme l'élémentalisme, la géomancie, la magie des ombres, la magie des runes et même la magie omniprésente pour donner un aspect magique à chaque lieu et et chaque créature dans vos parties.

Date de sortie US : 08/2021

{s:LivrePF2|https://cdn.paizo.com/image/product/catalog/PZO/PZO2109_180.png|Guns & gears}
Quand les épées et les sorts ne suffisent plus pour remporter la victoire, il est temps de donner un coup de boost à vos parties avec des engrenages mécaniques, des spirales à électricité et de la poudre à canon ! ''Guns & Gears'' est un livre à couverture rigide pour Pathfinder 2e édition qui introduit les armes à feu et la technologie fantastique dans vos campagnes ! Découvrez les secrets des créatures mécaniques avec la nouvelle classe d'inventeur ou écrasez vos adversaires en tant qu'un pistoléro armé d'une arme à feu ! En plus de ces nouvelles classes, le livre contient une pléthore de nouveaux archétypes, backgrounds, véhicule, engins de siège, gadget et la nouvelle ascendance d'automate, des options prêtes à venir enrichir les champs de bataille, petits et grands, de vos parties !

''Guns & Gears'' comporte : deux nouvelles classes (l'intelligent inventeur et le pistoléro), l'ascendance d'automate pour les joueurs qui veulent incarner une créature artificielle personnalisable, des armes à feu de tous genres (du simple et efficace pistolet à silex aux lames-fusils polyvalentes), des dizaines de nouveaux archétypes, des douzaines de nouveaux gadgets et véhicules, des engins de siège et les règles qui permettent de les gérer, ainsi qu'un article révélant comment les armes à feu et la technologie peuvent être insérées dans l'âge des Présages perdus de Golarion, y compris un coup d'oeil à la technologie des continents de l'Arcadie et du Tian Xia et des secrets encore inédits concernant la ville impitoyable et agitée d'Alkenastre.

Date de sortie US : 09/2021


{s:LivrePF2|https://cdn.paizo.com/image/product/catalog/PZO/PZO2110_180.png|Book of the Dead}
Les morts se relèvent ! Ce bouquin rempli d'informations blasphématoires offre aux joueurs et aux MJ tout ce dont ils ont besoin pour incorporer des menaces mortes-vivantes au sein de leurs aventures Pathfinder. Le livre inclut des outils pour combattre les nombreux morts-vivants mais aussi des options permettant aux joueurs eux-mêmes de contrôler ou même de devenir des créatures mortes-vivantes. Les MJ y trouveront de nouveaux objets et de nouvelles hantises, ainsi que des informations sur les contrées du monde de campagne des Présages Perdus infestées de morts-vivants. Une épaisse section de type bestiaire regorgeant de morts-vivants ajoute de nouveaux dangers que les MJ pourront utiliser et de nouvelles créatures conjurables pour les PJ, y compris des versions supplémentaires de morts-vivants classiques comme les vampires, squelettes et zombies. Ce livre cartonné de 224 page comporte également une aventure entière sur le thème du combat contre les morts-vivants.

Date de sortie US : 03/2022


{s:LivrePF2|https://cdn.paizo.com/image/product/catalog/PZO/PZO2111_180.png|Dark Archive}
Au-delà des frontières de la connaissance, l'inconnu vous appelle. Dark Archive renferme des secrets que tout joueur ou MJ peut utiliser pour révéler le paranormal qui se tapit dans les parties PAthfinder ! Ce livre de règle cartonné de 224 pages présente deux nouvelles classes de personnages conçues pour plonger dans l'inexpliqué : laissez libre-cours aux pouvoirs incommensurables de votre esprit en tant que psychique ou utilisez des secrets surnaturels et des instruments mystiques en tant que thaumaturge !

En plus de ces deux classes, 8 dossiers secrets offrent des options de joueur, des outils de MJ et du savoir relatif à un sujet paranormal y compris : d'étranges cryptides aperçus dans la nuit (ainsi que l'équipement pour les traquer et les pouvoirs que vous pourriez acquérir en survivant à une rencontre), les cultes et les croyances ésotériques (comme la magie divine apocryphe et le secret permettant de devenir un vaisseau vivant prêt à accueillir un être venu d'ailleurs), ou les anomalies temporales (avec des archétypes qui surfent à la surface du temps et un nouveau mystère pour les oracles qui ne sont pas contraints par les causalités habituelles). Chaque dossier contient également une aventure courte permettant de plonger les joueurs dans le paranormal, de voyager à travers Golarion ; jouez-les toutes les huit pour découvrir des phénomènes inexplicables de l'Âge des Présages Perdus !

Date de sortie US : 07/2022



{s:ClearRight}
===Bestiaires et livres relatifs à des créatures===

{s:LivrePF2|https://cdn.paizo.com/image/product/catalog/PZO/PZO2102_180.jpeg|Bestiaire 1}
Ce livre présente plus de 400 créatures à utiliser dans le jeux de rôle Pathfinder. Vous pouvez trouver des ennemis familiers comme les orcs, les dragons et les vampires mais aussi de nouvelles horreurs comme le nilith, le mukradi à 3 têtes, ainsi que des serviteurs pour les invocateurs de tout alignement.

Date de sortie US : 01/08/2019 — Date de sortie FR : 01/03/2020

{s:LivrePF2|https://cdn.paizo.com/image/product/catalog/PZO/PZO2104_180.jpeg|Bestiaire 2}
Ce livre présente plus de 350 créatures à utiliser dans le jeux de rôle Pathfinder. Des créatures classiques en revenant vers les monstres préférés comme les dragons primaux, à de nouvelles menaces qui mettront à l'épreuve les plus courageux des héros. Ce livre de monstres à été conçu pour défier les personnages de tous niveaux.

Date de sortie US : 27/05/2020 — Date de sortie FR : Août 2021

{s:LivrePF2|https://cdn.paizo.com/image/product/catalog/PZO/PZO2107_180.png|Bestiaire 3}
Avec plus de 300 monstres à la fois classiques et tout nouveaux, ce livre de règles à couverture rigide de 320 pages complète la collection de créatures commencées avec les deux premiers volumes du Bestiaire. Ce recueil de monstres indispensable conçu pour défier des personnages de tous niveaux est un compagnon essentiel pour vos parties de Pathfinder, avec des créatures classiques telles que les créatures mécaniques et les fées voleuses de dents, des monstres plébiscités tels que les dragons impériaux et les puissants titans ainsi que de tout nouveaux dangers provenant de tous les coins de Golarion.

Date de sortie US : 07/04/2021

{s:LivrePF2|https://cdn.paizo.com/image/product/catalog/PZO/PZO9311_180.png|Lost Omens: Monsters of Myth}
Lâchez les bêtes ! Nombreux sont les héros de l'âge des Prédictions perdues mais, pour chaque grand héros, il existe un monstre encore plus puissant. Ce livre détaille 20 des monstres les plus infâmes et terrifiants de la région de la mer Intérieure et au-delà. Découvrez les secres de certains des monstres les plus célèbres de Golarion, du Diable de Pointesable à Fafnheir, le Père de tous les linnorms. Monsters of Myth (Monstres mythiques) propose des rumeurs, des récits et même des trésors pour les braves aventuriers prêts à affronter ces créatures légendaires !

Date de sortie US : 11/2021

{s:ClearRight}
===Lost Omens / Monde des Présages perdus===

{s:LivrePF2|https://cdn.paizo.com/image/product/catalog/PZO/PZO9301_180.jpeg|Pathfinder Lost Omens World Guide / Guide du monde des prédictions perdues}
Ce guide indispensable de 136 pages sur le monde de Pathfinder présente tout ce que vous devez savoir pour une vie d'aventure dans l'age des prédictions perdues. Le dieu des humains est mort et la prophétie est brisée, laissant les aventuriers comme vous se forger leur propre destin dans un avenir incertain!

Cet atlas comprend 10 régions regorgeant de possibilités passionnantes et mortelles et est accompagné d'une carte géante à double face.

Date de sortie US : 28/08/2019 — Date de sortie FR : 12/2020

{s:LivrePF2|https://cdn.paizo.com/image/product/catalog/PZO/PZO9302_180.jpeg|Pathfinder Lost Omens Character Guide / Guide des personnages des prédictions perdues}
La vie d'un aventurier peut être difficile, les longs voyages et les lourds fardeaux sont plus supportables lorsque vous êtes en bonne compagnie. Ce guide du monde de Pathfinder présente les peuples et les organisations qui peuvent aider ou gêner des héros comme vous!

Ce guide de 136 pages propose 10 nouveaux héritages, près de 100 nouveaux dons pour les ascendances existantes, ainsi que trois nouvelles ascendances avec les hobgobelins, les lahys, et les Hommes-Lézards  . Rejoignez cinq des factions les plus influentes de Golarion, combattez à leurs côtés ou affrontez-les!

Date de sortie US : 16/10/2019 — Date de sortie FR : 03/2021

{s:LivrePF2|https://cdn.paizo.com/image/product/catalog/PZO/PZO9303_180.jpeg|Pathfinder Lost Omens: Gods & Magic}
Aucun univers fantastique n'est complet sans un panthéon de divinités puissantes que ses personnages vénéreraient ou craindraient. Que vous soyez un voyou demandant au dieu du vol une bénédiction lors de votre prochain braquage ou un valeureux paladin appelant la puissance de votre dieu contre les forces du mal, la foi est la clé de l'identité de chaque personnage. Dans ce volume, vous trouverez des détails sur les dieux et les confessions non déiques de l'âge des prédictions perdues du point de vue de leur clergé et de leurs fidèles. Vous découvrirez également de nouveaux domaines, dons et sorts pour personnaliser votre personnage, et un index exhaustif de centaines de divinités que vous pouvez vénérer (et les avantages de le faire).

Ce livre de 128 pages est fait pour les joueurs cherchant à étoffer les motivations de leurs personnages et les Maîtres du Jeu cherchant à donner vie aux sectes maléfiques, aux évangélistes zélés et aux guerriers saints de leurs campagnes.

Date de sortie US : 29/01/2020 — Date de sortie FR : Février 2022

{s:LivrePF2|https://cdn.paizo.com/image/product/catalog/PZO/PZO9306_180.jpeg|Pathfinder Lost Omens: Legends}
Le plus important n'est pas toujours ''ce'' que vous connaissez mais ''qui'' vous connaissez. Les héros de l'Âge des Présages perdus forgent leur destin dans ce monde incertain mais ce monde a été façonné par bien d'autres qui les ont précédés ou qui agissent actuellement, en même temps qu'eux. Les Légendes du Monde des Présages perdus contient des informations sur 42 des personnalités les plus importantes de la région de la mer Intérieure, qu'il s'agisse de reines et de rois dirigeant le présent ou d'individus provenant du lointain passé de Golarion. Découvrez des détails sur la vie de ces personnes qui influencent le monde tout autour du globe, ainsi que des techniques secrètes, des objets et des savoirs que les PJ pourraient obtenir en rencontrer ces individus hors du commun !

Date de sortie US : 30/07/2020

{s:LivrePF2|https://cdn.paizo.com/image/product/catalog/PZO/PZO9307_180.jpeg|Pathfinder Lost Omens: Pathfinder Society Guide}
La Société des Éclaireurs et une organisation de globe-trotters, d'aventuriers, d'érudits et de combattants qui dévouent leur vie à explorer, à collecter des savoirs et des trésors perdus, et à les partager avec le monde. Le Guide de la Société des Éclaireurs détaille tout ce que les joueurs et les MJ ont besoin de savoir au sujet de la Société, qu'il s'agisse de comment devenir un membre, des diverses factions de la Société ou des loges parsemées à travers la région de la mer Intérieure. Ce livre est la référence en matière d'histoire et de savoir au sujet de la Société des Éclaireurs. Il comporte de nouveaux éléments de règles y compris de l'équipement, des boussoles d'Éclaireurs et de nouvelles options pour les archétypes liés aux Éclaireurs. Un livre déjà bien utile de lui-même, mais un must-have pour les participants au jeu organisé de la Société des Éclaireurs et une excellente manière de rejoindre cette campagne internationale !

Date de sortie US : 14/10/2020

{s:LivrePF2|https://cdn.paizo.com/image/product/catalog/PZO/PZO9308_180.png|Pathfinder Lost Omens: Ancestry Guide}
Souvenez-vous de qui vous êtes ! L'Âge des Présages perdus est rempli d'individus de tous types, bien plus que juste les ascendances communes. Le Guide des ascendances du Monde des Présages perdus s'intéresse aux ascendances peu communes et rares de la mer Intérieure (comme les descendants des génies, les androïdes, les kitsunes, les esprits et plus encore !), offrant des informations sur leurs cultures et la place qu'elles occupent dans le monde. Le livre complète également les options de règles pour ces ascendances et pour les héritages polyvalents. Finalement, le Guide des Ascendances propose également de nouvelles ascendances et de nouveaux héritages polyvalents, y compris certains qui sont tout nouveaux, mais aussi d'anciens favoris provenant de partout sur Golarion !

Date de sortie US : 24/02/2021

{s:LivrePF2|https://cdn.paizo.com/image/product/catalog/PZO/PZO9309_180.png|Pathfinder Lost Omens: The Mwangi Expanse}
Au sud d'un chaîne de montagnes infranchissable s'étend un territoire aux nombreuses ressources et aux innombrables opportunités. L'Étendue mwangi accueille une incroyable diversité de cultures et de peuples depuis des temps immémoriaux, possède de puissantes cités-états isolées qui ne se sont bien souvent guère préoccupées de leurs voisins. Mais le destin a amené des changements qui ont eu des répercussions à travers tout le monde. Alors qu'un groupe d'érudits provenant d'une université ancestrale s'aventurent vers le nord pour venir en aide à une Avistan dévasté par un désastre, une nation forgée dans la révolution cherche de puissants alliés contre les agressions étrangères, et un dieu mort-vivant qui était autrefois un symbole d'espoir pour une nation sur le déclin voit sa jalousie s'accroître au point de s'en prendre à ceux qu'il prétendait être son propre peuple.

Que vous soyez un diplomate à la recherche d'appuis à utiliser dans les situations difficiles, un espion constamment à l'aguêt du moindre signe de danger dans son environnement, un protecteur espérant défendre sa demeure et son peuple ou un combattant cherchant à repousser des tyrans locaux ou étrangers, ce guide de l'Étendue mwangie vous offre les ressources nécessaires pour explorer un royaume de magie, de monstres et d'intrigue !

Date de sortie US : 07/07/2021

{s:LivrePF2|https://cdn.paizo.com/image/product/catalog/PZO/PZO9310_180.png|Pathfinder Lost Omens: The Grand Bazar}
Soyez le premier à partir au marché avec ''Lost Omens: The Grand Bazaar'' ! Le Grand Bazar est le plus grand marché d'Absalom, sur lequel on trouve des objets provenant de partout sur Golarion. Ici, un aventurier peut trouver toutes sortes d'équipements pouvant l'aider au cours de sa prochaine quête : armes, armures, objets magiques, objets d'accessibilité pour rendre l'aventure possible pour tout le monde, nouveaux compagnons animaux et plus encore ! Sur le Grand Bazar se trouvent également d'innombrables marchands et échoppes uniques. Les MJ pourront les utiliser dans leurs campagnes pour détailler leur monde ou inspirer de nouvelles aventures. Jetez un coup d'oeil sur ce qui est disponible sur le Grand Bazar et profitez des bonnes affaires tant que vous le pouvez !

Date de sortie US : 08/2021

{s:LivrePF2|https://cdn.paizo.com/image/product/catalog/PZO/PZO9304_180.jpeg|Pathfinder Lost Omens: Absalom, City of Lost Omens}
Depuis presque 5 000 années, la grande cité d'Absalom s'est dressée comme centre culturel, commercial et prophétique de la mer Intérieure. Aujourd'hui, avec la mort du dieu fondateur de la ville, Aroden, la disparition du seigneur-maire de la ville et les récentes attaques lancées par certains de ses pires ennemis, Absalom est au seuil d'une nouvelle destinée incertaine.

Ce guide à couverture rigide de 296 pages est rempli d'informations au sujet des lieux, des habitants et des aventures dans la ville la plus célèbre de Pathfinder. Il s'agit du recueil le plus large jamais écrit sur une des villes de Pathfinder à ce jour. Il présente un lieu fascinant qui peut accueillir des années d'aventures ! Un gigantesque poster en 8 panneaux représente la ville avec des détails jamais publiés auparavant, de manière à permettre à vos héros de sillonner les rues pavées avec des siècles d'histoire, de suivre les pas de générations d'aventuriers afin de se forger un nouveau destin pour la Ville au Centre du Monde !

Date de sortie US : 11/2021

{s:LivrePF2|https://cdn.paizo.com/image/product/catalog/PZO/PZO9312_180.png|Pathfinder Lost Omens: Knights of Lastwall}
Respectez votre serment ! Quand les armées du Tyran qui Murmure ont marché sur Dernier-Rempart, les places-fortes et les cités sont peut-être tombés, mais les habitants et leurs convictions sont restés fermes ! Ce livre offre un compte-rendu détaillé des chevaliers qui ont pris les armes pour poursuivre le combat contre les hordes de morts-vivants et les créatures maléfiques qui sillonnent le monde. Il décrit l'organisation des Chevaliers de Dernier-Rempart, comment les rejoindre, comment s'entraîner à devenir un chevalier, en passant par les missions visant à protéger les innocents et à vaincre les créatures maléfiques. Ce livre présente également de nouvelles règles telles que de l'équipement, des objets magiques, des sorts et des options supplémentaires pour les archétypes liés aux chevaliers de Dernier-Rempart pour les joueurs qui voudraient incarner un chevalier dans une campagne.

Date de sortie US : 04/2022

{s:LivrePF2|https://cdn.paizo.com/image/product/catalog/PZO/PZO9314_180.png|Pathfinder Lost Omens: Impossible Lands}
Sur ces terres portant encore les traces d'une guerre qui a opposé deux mages très puissants et les nations qu'ils dirigent, la réalité n'est pas limitée par les règles qui s'imposent dans le reste du monde. Des cités construites via des souhaits et des champs labourés par des morts animés séparent des territoires où la magie est déformée et semble parfois s'animer de sa propre volonté impénétrable. Explorez l'histoire des rois-magiciens immortels, maniez des explosifs et des technologies rares, et prenez part à des légendes à couper le souffle dans une région où le présent reste hanté par le passé et les échos de la destruction continuent de faire trembler les esprits et les âmes de ceux qui sont suffisamment courageux pour s'aventurer dans les Terres impossibles !

Date de sortie US : 10/2022
