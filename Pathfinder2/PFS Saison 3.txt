---
Name: Pathfinder2.PFS Saison 3
Title: PFS Saison 3
LastModified: 2021-06-14 18:29
---
{s:Image|Logos/PFS2/Year of Shattered Sanctuaries.png|240px|fright}
• [[MainPage|Retour à la page principale pour Pathfinder 2]] <br/>
• [[PFS|Retour à la page PFS pour Pathfinder 2]]

=== Année des Sanctuaires Brisés ===
{s:LivrePF2|https://cdn.paizo.com/image/product/catalog/PZOP/PZOPFS0301E.jpg|Introduction to Pathfinder Society : Year of Shattered Sanctuaries}
'''Titre VF''' <br/>
'''Niveau''' 1-4, par Mike Kimmel<br/>
'''Tag''' Intrigue principale, Répétable<br/>
'''Lieu''' ?

A recently inducted Pathfinder field agent makes a startling discovery as pleas for assistance flock to the Society’s Grand Lodge from across the Inner Sea. A group of Pathfinders come to assist in parsing the ongoing damage to the Society when a staunch ally requests their assistance. Stepping into a problem close to home, the agents quickly uncover a larger plot set to tear the Society apart and that it’s up to them to raise the alarm!
